package uk.gov.homeoffice.tdcs.cte.trainline.stepdefs;

import org.openqa.selenium.WebDriver;

import uk.gov.homeoffice.tdcs.cte.trainline.model.*;
import uk.gov.homeoffice.tdcs.cte.utils.DriverFactory;
import uk.gov.homeoffice.tdcs.cte.utils.PropertyReader;

import java.util.List;

import org.junit.Assert;

import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class TrainlineStepDefs{
	private class SearchCriteria {
		public String from;
		public String to;
		public String oneWay;
		public Integer outDate;
		public String nextDayReturn;
	}

	TrainlineHomepage theTrainLineHomePage;
	SearchResults searchResults;

	private WebDriver driver;

	@Given("^the Trainline site is accessed$")
	public void the_TrainLine_site_is_accessed() throws Throwable {
		System.setProperty("webdriver.chrome.driver", "C:\\tools\\selenium\\drivers\\chromedriver.exe");
		String baseURL = new PropertyReader().readProperty("url");
		driver.get(baseURL);

		theTrainLineHomePage = new TrainlineHomepage(driver);
		Assert.assertEquals("Title of main logo mismatch", "Trainline - Finding you the cheapest tickets",
				theTrainLineHomePage.getLogoTitleText());
	}

	@When("^searching for trains$")
	public void searching_for_trains(List<SearchCriteria> criteria) throws Throwable {
		SearchCriteria searchCriteria = criteria.get(0);

		theTrainLineHomePage.setFromInputText(searchCriteria.from);
		theTrainLineHomePage.setToInputText(searchCriteria.to);
		theTrainLineHomePage.setOneWay(searchCriteria.oneWay);
		theTrainLineHomePage.setOutDate(searchCriteria.outDate);
		Thread.sleep(1000);

		theTrainLineHomePage.clickNextDayLink(searchCriteria.nextDayReturn);
		searchResults = theTrainLineHomePage.clickSearch();

	}

	@Then("^the Search results page is displayed$")
	public void the_Search_results_page_is_displayed() throws Throwable {
		Assert.assertEquals("Out table header not found", "OUT", searchResults.getOutTableHeaderText());

	}

	@After
	public void afterScenario() throws Throwable {
		new DriverFactory().destroyDriver();
	}

	@Before
	public void beforeScenario() throws Throwable {
		driver = new DriverFactory().getDriver();
	}
}
