package uk.gov.homeoffice.tdcs.cte.trainline.testrunner;

import org.junit.runner.RunWith;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(
    features = "src/test/resources/features/JourneySearch.feature",
    glue = "uk.gov.homeoffice.tdcs.cte.trainline.stepdefs",
    tags = "@tag",
    plugin = {"pretty", "html:test-results/thetrainline", "json:test-results/thetrainline.json"}
)

public class TestRunner {

}
