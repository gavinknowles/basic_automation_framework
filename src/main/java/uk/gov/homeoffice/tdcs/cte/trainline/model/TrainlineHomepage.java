package uk.gov.homeoffice.tdcs.cte.trainline.model;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;


public class TrainlineHomepage {

	WebDriver driver;

	@FindBy(xpath = "//*[@id='master']/header/div/div/div[1]/a")
	public WebElement LOGO_TITLE;

	@FindBy(xpath = ".//*[@id='originStation']")
	public WebElement FROM_TEXT_BOX;

	@FindBy(xpath = ".//*[@id='destinationStation']")
	public WebElement TO_TEXT_BOX;

	@FindBy(xpath = ".//*[@id='isOneWay']")
	public WebElement ONEWAY_CHECK_BOX;

	@FindBy(xpath = ".//*[contains(text(), 'Tomorrow')]")
	public WebElement TOMORROW_LINK_TEXT;

	@FindBy(xpath = ".//*[contains(text(), 'Next day')]")
	public WebElement NEXTDAY_LINK_TEXT;

	@FindBy(xpath = ".//*[@id='submitButton']")
	public WebElement SUBMIT_BUTTON;

	@FindBy(xpath = ".//*[@id='outDate']")
	public WebElement OUTDATE_CALENDAR;

	public TrainlineHomepage(WebDriver driver) {
		this.driver = driver;
		// Homepage object has been created.

		PageFactory.initElements(driver, this);

	}

	public void setFromInputText(String value) {
		FROM_TEXT_BOX.sendKeys(value);
	}

	public void setToInputText(String value) {
		TO_TEXT_BOX.sendKeys(value);
	}

	public void setOneWay(String value) {
		if (value.equals("No") && ONEWAY_CHECK_BOX.isSelected()) {
			ONEWAY_CHECK_BOX.click();
		}

		if (value.equals("Yes") && !ONEWAY_CHECK_BOX.isSelected()) {
			ONEWAY_CHECK_BOX.click();
		}
	}

	public String getLogoTitleText() {
		return LOGO_TITLE.getAttribute("title");
	}

	public SearchResults clickSearch() {
		SUBMIT_BUTTON.click();
		return new SearchResults(this.driver);
	}

	public void clickNextDayLink() {
		NEXTDAY_LINK_TEXT.click();
	}

	public void clickNextDayLink(String nextDayReturn) {
		if (nextDayReturn.equals("Yes")) {
			clickNextDayLink();
		}
	}

	public void setOutDate(Integer daysInFuture) {
		Boolean dateFound = false;
		SimpleDateFormat sdf;

		// Set the out date
		Calendar calendar = Calendar.getInstance();

		calendar.setTime(new Date());
		calendar.add(Calendar.DATE, daysInFuture);

		// Setup the text to find in the calendar
		sdf = new SimpleDateFormat("MMMM yyyy");
		String headerToFind = sdf.format(calendar.getTime());
		sdf = new SimpleDateFormat("d");
		String dayToFind = sdf.format(calendar.getTime());

		OUTDATE_CALENDAR.click();

		while (dateFound == false) {
			// get a list of all the datepicker boxes and iterate them
			List<WebElement> calendars = driver.findElements(By.xpath("//*[@id='ui-datepicker-div']/*"));

			for (WebElement cal : calendars) {
				if (cal.getText().contains(headerToFind)) {
					cal.findElement(By.linkText(dayToFind)).click();
					dateFound = true;
					break;
				}
			}

			// If we didn't find the date then click next
			if (dateFound == false) {
				Boolean isPresent = driver.findElements(By.xpath("//*[@id='ui-datepicker-div']/div[2]/div/a/span"))
						.size() > 0;
						if (isPresent == true) {
							driver.findElement(By.xpath("//*[@id='ui-datepicker-div']/div[2]/div/a/span")).click();
						} else {
							break;
						}
			}
		}

		if (dateFound == false) {
			throw new IllegalStateException("Date [" + calendar.toString() + "] could not be selected");
		}
	}
}
