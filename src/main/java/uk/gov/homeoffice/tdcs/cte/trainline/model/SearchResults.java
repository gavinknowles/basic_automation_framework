package uk.gov.homeoffice.tdcs.cte.trainline.model; 

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class SearchResults {
	WebDriver driver;

	@FindBy (xpath = "//*[@id='tickets']/div/div[1]/table/thead/tr[1]/th[2]/div/div[1]")
	WebElement OUT_TABLE_HEADER;
	
	public SearchResults(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	public String getOutTableHeaderText() {
		return OUT_TABLE_HEADER.getText();
	}
}
