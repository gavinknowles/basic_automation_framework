
@tag
Feature: Trainline Journey Search
  As a user
  I want to use the Trainline website
  So I can search for train journeys

  Background: 
    Given the Trainline site is accessed

  Scenario: Basic Journey Search
    When searching for trains
      | from     | to     | oneWay | outDate | nextDayReturn |
      | Brighton | London | No     |      60 | Yes           |
    Then the Search results page is displayed

  Scenario: Advanced Journey Search
    When searching for trains
      | from     | to     | oneWay | outDate | nextDayReturn |
      | Brighton | Southdampton | No     |      90 | Yes           |
    Then the Search results page is displayed