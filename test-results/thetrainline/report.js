$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("src/test/resources/features/JourneySearch.feature");
formatter.feature({
  "line": 3,
  "name": "Trainline Journey Search",
  "description": "As a user\r\nI want to use the Trainline website\r\nSo I can search for train journeys",
  "id": "trainline-journey-search",
  "keyword": "Feature",
  "tags": [
    {
      "line": 2,
      "name": "@tag"
    }
  ]
});
formatter.before({
  "duration": 4393589495,
  "status": "passed"
});
formatter.background({
  "line": 8,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 9,
  "name": "the Trainline site is accessed",
  "keyword": "Given "
});
formatter.match({
  "location": "TrainlineStepDefs.the_TrainLine_site_is_accessed()"
});
formatter.result({
  "duration": 4633438619,
  "status": "passed"
});
formatter.scenario({
  "line": 11,
  "name": "Basic Journey Search",
  "description": "",
  "id": "trainline-journey-search;basic-journey-search",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 12,
  "name": "searching for trains",
  "rows": [
    {
      "cells": [
        "from",
        "to",
        "oneWay",
        "outDate",
        "nextDayReturn"
      ],
      "line": 13
    },
    {
      "cells": [
        "Brighton",
        "London",
        "No",
        "60",
        "Yes"
      ],
      "line": 14
    }
  ],
  "keyword": "When "
});
formatter.step({
  "line": 15,
  "name": "the Search results page is displayed",
  "keyword": "Then "
});
formatter.match({
  "location": "TrainlineStepDefs.searching_for_trains(TrainlineStepDefs$SearchCriteria\u003e)"
});
formatter.result({
  "duration": 9085672651,
  "status": "passed"
});
formatter.match({
  "location": "TrainlineStepDefs.the_Search_results_page_is_displayed()"
});
formatter.result({
  "duration": 98419733,
  "status": "passed"
});
formatter.after({
  "duration": 870084869,
  "status": "passed"
});
formatter.before({
  "duration": 1196281106,
  "status": "passed"
});
formatter.background({
  "line": 8,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 9,
  "name": "the Trainline site is accessed",
  "keyword": "Given "
});
formatter.match({
  "location": "TrainlineStepDefs.the_TrainLine_site_is_accessed()"
});
formatter.result({
  "duration": 4910776168,
  "status": "passed"
});
formatter.scenario({
  "line": 17,
  "name": "Advanced Journey Search",
  "description": "",
  "id": "trainline-journey-search;advanced-journey-search",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 18,
  "name": "searching for trains",
  "rows": [
    {
      "cells": [
        "from",
        "to",
        "oneWay",
        "outDate",
        "nextDayReturn"
      ],
      "line": 19
    },
    {
      "cells": [
        "Brighton",
        "Southdampton",
        "No",
        "90",
        "Yes"
      ],
      "line": 20
    }
  ],
  "keyword": "When "
});
formatter.step({
  "line": 21,
  "name": "the Search results page is displayed",
  "keyword": "Then "
});
formatter.match({
  "location": "TrainlineStepDefs.searching_for_trains(TrainlineStepDefs$SearchCriteria\u003e)"
});
formatter.result({
  "duration": 2340318089,
  "status": "passed"
});
formatter.match({
  "location": "TrainlineStepDefs.the_Search_results_page_is_displayed()"
});
formatter.result({
  "duration": 24846369,
  "error_message": "org.openqa.selenium.NoSuchElementException: no such element: Unable to locate element: {\"method\":\"xpath\",\"selector\":\"//*[@id\u003d\u0027tickets\u0027]/div/div[1]/table/thead/tr[1]/th[2]/div/div[1]\"}\n  (Session info: chrome\u003d51.0.2704.103)\n  (Driver info: chromedriver\u003d2.21.371459 (36d3d07f660ff2bc1bf28a75d1cdabed0983e7c4),platform\u003dWindows NT 10.0 x86_64) (WARNING: The server did not provide any stacktrace information)\nCommand duration or timeout: 23 milliseconds\nFor documentation on this error, please visit: http://seleniumhq.org/exceptions/no_such_element.html\nBuild info: version: \u00272.53.0\u0027, revision: \u002735ae25b1534ae328c771e0856c93e187490ca824\u0027, time: \u00272016-03-15 10:43:46\u0027\nSystem info: host: \u0027SurfaceBook\u0027, ip: \u0027192.168.43.137\u0027, os.name: \u0027Windows 10\u0027, os.arch: \u0027amd64\u0027, os.version: \u002710.0\u0027, java.version: \u00271.8.0_77\u0027\nDriver info: org.openqa.selenium.chrome.ChromeDriver\nCapabilities [{applicationCacheEnabled\u003dfalse, rotatable\u003dfalse, mobileEmulationEnabled\u003dfalse, chrome\u003d{chromedriverVersion\u003d2.21.371459 (36d3d07f660ff2bc1bf28a75d1cdabed0983e7c4), userDataDir\u003dC:\\Users\\gavin\\AppData\\Local\\Temp\\scoped_dir2340_8280}, takesHeapSnapshot\u003dtrue, databaseEnabled\u003dfalse, handlesAlerts\u003dtrue, hasTouchScreen\u003dfalse, version\u003d51.0.2704.103, platform\u003dXP, browserConnectionEnabled\u003dfalse, nativeEvents\u003dtrue, acceptSslCerts\u003dtrue, locationContextEnabled\u003dtrue, webStorageEnabled\u003dtrue, browserName\u003dchrome, takesScreenshot\u003dtrue, javascriptEnabled\u003dtrue, cssSelectorsEnabled\u003dtrue}]\nSession ID: c90d5e71e21085b9cf476b36dd7e8891\n*** Element info: {Using\u003dxpath, value\u003d//*[@id\u003d\u0027tickets\u0027]/div/div[1]/table/thead/tr[1]/th[2]/div/div[1]}\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance0(Native Method)\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance(NativeConstructorAccessorImpl.java:62)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(DelegatingConstructorAccessorImpl.java:45)\r\n\tat java.lang.reflect.Constructor.newInstance(Constructor.java:423)\r\n\tat org.openqa.selenium.remote.ErrorHandler.createThrowable(ErrorHandler.java:206)\r\n\tat org.openqa.selenium.remote.ErrorHandler.throwIfResponseFailed(ErrorHandler.java:158)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.execute(RemoteWebDriver.java:678)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElement(RemoteWebDriver.java:363)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElementByXPath(RemoteWebDriver.java:500)\r\n\tat org.openqa.selenium.By$ByXPath.findElement(By.java:361)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElement(RemoteWebDriver.java:355)\r\n\tat org.openqa.selenium.support.pagefactory.DefaultElementLocator.findElement(DefaultElementLocator.java:69)\r\n\tat org.openqa.selenium.support.pagefactory.internal.LocatingElementHandler.invoke(LocatingElementHandler.java:38)\r\n\tat com.sun.proxy.$Proxy15.getText(Unknown Source)\r\n\tat uk.gov.homeoffice.tdcs.cte.trainline.model.SearchResults.getOutTableHeaderText(SearchResults.java:20)\r\n\tat uk.gov.homeoffice.tdcs.cte.trainline.stepdefs.TrainlineStepDefs.the_Search_results_page_is_displayed(TrainlineStepDefs.java:61)\r\n\tat ✽.Then the Search results page is displayed(src/test/resources/features/JourneySearch.feature:21)\r\n",
  "status": "failed"
});
formatter.after({
  "duration": 764104519,
  "status": "passed"
});
});